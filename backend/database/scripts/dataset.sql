
INSERT INTO `days` (`id`, `value`, `order`)
VALUES (1,'Lunes',1), (2,'Martes',2), (3,'Miercoles',3), (4,'Jueves',4), (5,'Viernes',5);

INSERT INTO `hours` (`id`, `value`, `order`)
VALUES (1,'6:45-8:15',1), (2,'8:15-9:45',2), 
(3,'9:45-11:15',3), (4,'11:15-12:45',4), (5,'12:45-14:15',5),
(6,'14:15-15:45',6), (7,'15:45-17:15',7), (8,'17:15-18:45',8), 
(9,'18:45-20:15',9), (10,'20:15-21:45',10);

INSERT INTO user
VALUES (100001, "juan.perez@univ.edu", "Perez Perez", "Juan Alberto", "Password123!", "Admin", "201000125"),
(100002, "maria.perez@univ.edu", "Perez Soleto", "Maria Andrea", "Password123!", "Docente", "201000126"),
(100003, "marcos.montan@univ.edu", "Montan Quispe", "Marcos Alberto", "Password123!", "Estudiante", "205000125"),
(100004, "lucia.zurita@univ.edu", "Surita Mance", "Lucia Marta", "Password123!", "Auxiliar", "201005125")