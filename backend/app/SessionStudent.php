<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionStudent extends Model
{
    protected $table = 'session_student';

    public function Session()
    {
        return $this->belongsTo(Session::class);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }
}
