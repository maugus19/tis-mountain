<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Session;

class SessionController extends Controller
{
    public function index()
    {
        return Session::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session = Session::create(['group_id' => $request->groupId, 'schedule_id' => $request->scheduleId]);
        return response()->json($session, 200);
    }

    public function delete($id)
    {
        $session = Session::where('id', $id)->delete();
        return response()->json($session, 201);
    }

    public function getByGroup($id)
    {    
        $sessions = Session::where('group_id', $id)->with('schedule.day')->with('schedule.hour')->with('schedule.laboratory')->get();
        return response()->json($sessions, 201);
    }
    
}
