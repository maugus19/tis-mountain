<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = ['name', 'code'];

    public function groups()
    {
        return $this->belongsToMany(Group::class);
    }
}
