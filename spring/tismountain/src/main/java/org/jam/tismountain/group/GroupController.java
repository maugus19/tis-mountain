/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jam.tismountain.group;

import java.util.List;
import javax.validation.Valid;
import org.jam.tismountain.schedule.Schedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Mauricio Rojas
 */
@RepositoryRestController
@RequestMapping("/groups")
public class GroupController {

    @Autowired
    private GroupService groupService;

    @Autowired
    private LocalValidatorFactoryBean validator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.addValidators(validator);
    }

    @PostMapping
    public ResponseEntity<Group> save(@RequestBody @Valid Group group) {
        return new ResponseEntity<>(groupService.save(group), HttpStatus.CREATED);
    }

    @GetMapping("/subject/{subjectId}")
    public List<Group> getGroupsBySubject(@PathVariable Long subjectId) {
        return groupService.getGroupsBySubject(subjectId);
    }

    @PostMapping("/{groupId}/event/add-schedule")
    public Group addSchedule(@PathVariable Long groupId, Schedule schedule) {
        return groupService.addSchedule(groupId, schedule);
    }

    @DeleteMapping("/{groupId}/event/remove-schedule")
    public Group removeSchedule(@PathVariable Long groupId, Schedule schedule) {
        return groupService.removeSchedule(groupId, schedule);
    }
}
