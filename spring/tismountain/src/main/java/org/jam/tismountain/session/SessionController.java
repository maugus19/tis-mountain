/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jam.tismountain.session;

import java.util.List;
import javax.validation.Valid;
import org.jam.tismountain.group.GroupService;
import org.jam.tismountain.laboratory.Laboratory;
import org.jam.tismountain.session.sessionStudent.SessionStudent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author grymlock
 */
@RepositoryRestController
public class SessionController {

    @Autowired
    private SessionService sessionService;
    

    @Autowired
    private LocalValidatorFactoryBean validator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.addValidators(validator);
    }

    @PostMapping(path = "sessions")
    public ResponseEntity<Session> save(@RequestBody @Valid Session session) {
        return new ResponseEntity<>(sessionService.save(session), HttpStatus.CREATED);
    }

    @GetMapping(path = "sessions/group/{groupId}")
    public List<Session> getSessionsByGroup(@PathVariable Long groupId) {
        return sessionService.getSessionsByGroup(groupId);
    }
    
    @PostMapping(path = "users/{userId}/event/register-to-session")
    public SessionStudent registerToSession(@PathVariable Long userId, @Valid SessionStudent sessionStudent){
        return sessionService.registerStudentToSession(userId, sessionStudent);
    }
    
    @DeleteMapping(path = "sessions/{sessionId}")
    public Session deleteSession(@PathVariable Long sessionId){
        return sessionService.deleteSession(sessionId);
    }
}
