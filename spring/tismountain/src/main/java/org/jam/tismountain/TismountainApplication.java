package org.jam.tismountain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TismountainApplication {

	public static void main(String[] args) {
		SpringApplication.run(TismountainApplication.class, args);
	}

}
