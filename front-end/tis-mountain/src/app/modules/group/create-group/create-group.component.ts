import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../services/users.service';
import { SubjectService } from '../../../services/subject.service';
import { GroupService } from '../../../services/group.service';
import { Observable } from 'rxjs/Rx';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.component.html'
})
export class CreateGroupComponent implements OnInit {

  groupForm: FormGroup;
  teachers = [];
  subjects: any;

  constructor(private toastr: ToastrService,
    private userService: UsersService, 
    private subjectService: SubjectService,
    private groupService: GroupService,
    private formBuilder: FormBuilder,
    private router: Router) { 
      this.groupForm = this.createFormGroup();
    }

  ngOnInit() {
    let promiseGetAllUsers = this.userService.getAllUsers()
    let promiseGetAllSubjects = this.subjectService.getAllSubjects()
    Observable.forkJoin([promiseGetAllUsers, promiseGetAllSubjects])
       .subscribe((response) => {
        this.teachers = response[0]._embedded.users.filter(
          user => user.role === "Docente");
        this.subjects = response[1]._embedded.subjects;
       });
  }

  registerGroup(){
    this.groupService.registerGroup(this.groupForm.value)
    .then(data => {
      this.showMessage('Grupo registrado correctamente', 'alert alert-info');
      this.router.navigate(['/']);
    })
    .catch(error => {
      this.showMessage('Grupo no registrado', 'alert alert-warning');
    });
  }

  private createFormGroup(){
    return this.formBuilder.group({
      number: ['', [Validators.required, Validators.min(1), Validators.max(100)]],
      user_id: ['', Validators.required],
      subject_id: ['', Validators.required],
      laboratory_practice: ['']
    });
  }

  cancel(): void {
    this.router.navigate(['/']);
  }

  showMessage(message: string, toastClass: string) {
    this.toastr.info('<span class="now-ui-icons ui-1_bell-53"></span>' + message + '</b>.', '', {
      timeOut: 8000,
      closeButton: false,
      enableHtml: true,
      toastClass: toastClass,
      positionClass: 'toast-' + 'top' + '-' + 'right'
    });
  }

}
