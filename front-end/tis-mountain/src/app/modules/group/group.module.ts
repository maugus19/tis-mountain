import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CreateGroupComponent } from './create-group/create-group.component';
import { GroupService } from '../../services/group.service';
import { UsersService } from '../../services/users.service';
import { ScheduleService } from '../../services/schedule.service';
import { SubjectService } from '../../services/subject.service';
import { SessionService } from '../../services/session.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AdminGuardService } from '../../services/routes-guard.service';
import { AddScheduleComponent } from './add-schedule/add-schedule.component';
import { GroupListComponent } from './group-list/group-list.component';

const routes: Routes = [
  { path: '', component: CreateGroupComponent, canActivate: [AdminGuardService] },
  { path: 'add-schedule/:id', component: AddScheduleComponent, canActivate: [AdminGuardService] },
  { path: 'list', component: GroupListComponent, canActivate: [AdminGuardService] }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    GroupService,
    UsersService,
    SubjectService,
    ToastrService,
    ScheduleService,
    SessionService
  ],
  declarations: [CreateGroupComponent, AddScheduleComponent, GroupListComponent]
})
export class GroupModule { }
