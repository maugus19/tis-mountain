import { Component, ViewChild, OnInit } from '@angular/core';
import { GroupService } from '../../../services/group.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html'
})
export class GroupListComponent implements OnInit {
  groups = [];
  constructor(private toastr: ToastrService, private groupService: GroupService) { }

  ngOnInit() {
    this.groupService.getAllGroups()
    .then(data => {
      this.groups = data._embedded;
    });
  }

}
