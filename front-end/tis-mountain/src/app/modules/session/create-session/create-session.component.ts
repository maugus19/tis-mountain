import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

import { Session } from '../../../models/session';
import { Laboratory } from '../../../models/laboratory';
import { Group } from '../../../models/group';
import { SessionService } from '../../../services/session.service';
import { GroupService } from '../../../services/group.service';
import { LaboratoryService } from '../../../services/laboratory.service';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-create-session',
  templateUrl: './create-session.component.html'
})
export class CreateSessionComponent implements OnInit {

  session = new Session;
  laboratories: any;
  groups: any;
  days: any;
  hours: any;

  constructor(private service: SessionService, 
    private toastr: ToastrService, private router: Router,
    private groupService: GroupService, private laboratyService: LaboratoryService) { 
  }

  ngOnInit() { 
    let promiseAllLaboratory = this.laboratyService.getAllLaboratory()
    let promiseAllGroups = this.groupService.getAllGroups()
    this.days = ["Lunes",
    "Martes", "Miercoles", "Jueves", "Viernes"];
    this.hours = ["6:45-8:15", "8:15-9:45", "9:45-11:15", 
    "11:15-12:45", "12:45-14:15", "14:15-15:45", 
    "15:45-17:15", "17:15-18:45", "18:45-20:15", "20:15-21:45"];
    Observable.forkJoin([promiseAllLaboratory,promiseAllGroups])
       .subscribe((response) => {
        this.laboratories = response[0]      
        this.groups = response[1]
       });
  }

  saveSession(): void {
    this.service.registerSession(this.session)
    .then(data => {
      this.showMessage('Sesion registrado correctamente', 'alert alert-info alert-with-icon');
      this.router.navigate(['/']);
    })
    .catch(error => {
      this.showMessage('Sesion no registrado', 'alert alert-warning alert-with-icon');
    });
  }

  cancel(): void {
    this.router.navigate(['/']);
  }

  showMessage(message: string, toastClass: string) {
    this.toastr.info('<span class="now-ui-icons ui-1_bell-53"></span>' + message + '</b>.', '', {
      timeOut: 8000,
      closeButton: false,
      enableHtml: true,
      toastClass: toastClass,
      positionClass: 'toast-' + 'top' + '-' + 'right'
    });
  }
}
