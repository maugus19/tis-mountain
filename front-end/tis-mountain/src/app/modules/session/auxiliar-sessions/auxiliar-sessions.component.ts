import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auxiliar-sessions',
  templateUrl: './auxiliar-sessions.component.html',
  styleUrls: ['./auxiliar-sessions.component.css']
})
export class AuxiliarSessionsComponent implements OnInit {

  sessions = [
    {
      day: {value: "miercoles"},
      hour: {value: "14:15"},
      laboratory: {name: "Laboratorio de computo 1"},
      date: "22 de Mayo",
      subject: {name: "Introduccion a la programacion"}
    },
    {
      day: {value: "martes"},
      hour: {value: "17:15"},
      laboratory: {name: "Laboratorio de computo 1"},
      date: "21 de Mayo",
      subject: {name: "Introduccion a la programacion"}
    },
    {
      day: {value: "Lunes"},
      hour: {value: "18:45"},
      laboratory: {name: "Laboratorio de computo 1"},
      date: "20 de Mayo",
      subject: {name: "Introduccion a la programacion"}
    },
  ];
  constructor() { }

  ngOnInit() {
  }

}
