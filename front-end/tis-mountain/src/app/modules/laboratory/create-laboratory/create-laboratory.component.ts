import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { LaboratoryService } from '../../../services/laboratory.service';
import { Laboratory } from '../../../models/laboratory';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-create-laboratory',
  templateUrl: './create-laboratory.component.html'
})
export class CreateLaboratoryComponent implements OnInit {

  laboratory = new Laboratory;

  constructor(private service: LaboratoryService, private toastr: ToastrService, private router: Router) { }

  ngOnInit() { }

  saveLaboratory(): void {
    if (isNullOrUndefined(this.laboratory.name) || isNullOrUndefined(this.laboratory.machineType)
      || isNullOrUndefined(this.laboratory.capacity)) {
      this.showMessage('Campo obligatorio no llenado, recuerde llenar los marcados con (*)', 'alert alert-warning');
    } else {
      console.log(this.laboratory);
      this.service.registerLaboratory(this.laboratory).subscribe(
        res => {
          this.showMessage('Laboratorio registrado correctamente', 'alert alert-info');
          this.router.navigate(['/']);
        },
        err => {
          this.showMessage('Laboratorio no registrado', 'alert alert-warning ');
        });
    }

  }

  cancel(): void {
    this.router.navigate(['/']);
  }

  showMessage(message: string, toastClass: string) {
    this.toastr.info('<span class="now-ui-icons ui-1_bell-53"></span>' + message + '</b>.', '', {
      timeOut: 8000,
      closeButton: false,
      enableHtml: true,
      toastClass: toastClass,
      positionClass: 'toast-' + 'top' + '-' + 'right'
    });

  }
}
