export class User {
    id?: number;
    sisCode: number;
    name: string;
    lastName: string;
    email: string;
    role: string;
    password?: string;
    token?: string;
}
