import { Injectable } from '@angular/core';
import { UsersService } from './users.service';
@Injectable()
export class AuthService {

  roles = ['Admin', 'Docente', 'Auxiliar', 'Estudiante'];
  currentUser;
  DUMMY_ADMIN;
  DUMMY_PROFESOR;
  DUMMY_ASSISTANT;
  DUMMY_STUDENT;

  constructor(private userService: UsersService) {
    if (this.currentUser === undefined) {
      this.getTestUsers();
    }
  }

  getRoles() {
    return this.roles;
  }

  setCurrentUser(userRole) {
    switch (userRole) {
      case 'Admin': this.currentUser = this.DUMMY_ADMIN; break;
      case 'Docente': this.currentUser = this.DUMMY_PROFESOR; break;
      case 'Auxiliar': this.currentUser = this.DUMMY_ASSISTANT; break;
      case 'Estudiante': this.currentUser = this.DUMMY_STUDENT; break;
    }
  }

  getCurrentUser() {
    return this.currentUser;
  }

  //for dev pruposes only
  getTestUsers() {
      this.userService.getUserById(100001).subscribe(response => {
        this.DUMMY_ADMIN = response.json();
        this.userService.getUserById(100002).subscribe(response => {
          this.DUMMY_PROFESOR = response.json();
          this.userService.getUserById(100003).subscribe(response => {
            this.DUMMY_ASSISTANT = response.json();
            this.userService.getUserById(100004).subscribe(response => {
              this.DUMMY_STUDENT = response.json();
              this.currentUser = this.DUMMY_ADMIN;
            });
          });
        });
      });
  }
  // admin: 1, profesor: 2, student: 2, auxiliar: 3

  userIsAdmin() { return !!this.currentUser? this.currentUser.role === 'Admin' : false; }
  userIsProfessor() { return !!this.currentUser? this.currentUser.role === 'Docente' : false; }
  userIsAssistant() { return !!this.currentUser? this.currentUser.role === 'Auxiliar' : false;}
  userIsStudent() { return !!this.currentUser? this.currentUser.role === 'Estudiante' : false; }
}
