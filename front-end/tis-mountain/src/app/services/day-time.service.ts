import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class DayTimeService {
    route = 'http://localhost:8080';

    constructor(private http: Http) { }

    getDays() {
        return this.http.get(this.route + '/days')
        .map(res => {
            return res.json()
        }).toPromise();
    }

    getHours() {
        return this.http.get(this.route + '/hours')
        .map(res => {
            return res.json()
        })
        .toPromise();
    }
}
