import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Group } from '../models/group';

@Injectable()
export class GroupService {
    route = 'http://localhost:8080/groups';

    constructor(private http: Http) { }

    getAllGroups() {
        return this.http.get(this.route)
            .map(res => {
                return res.json()
            })
            .toPromise();
    }

    getGroupById(id: string) {
        return this.http.get(this.route + '/' + id)
            .map(res => {
                return res.json()
            })
            .toPromise();
    }

    getGroupsBySubject(subject) {
        return this.http.get(this.route + '/subject/' + subject.id)
        .map(res => {
            return res.json()
        })
        .toPromise();
    }

    registerGroup(group) {
        return this.http.post(this.route, group)
        .map(res => {
            return res.json()
        })
        .toPromise();
    }
}
