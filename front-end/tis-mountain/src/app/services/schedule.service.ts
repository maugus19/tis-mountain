import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class ScheduleService {
    route = 'http://localhost:8080/schedules';

    constructor(private http: Http) { }

    registerSchedule(schedule) {
        return this.http.post(this.route, schedule).toPromise();
    }

    getAllSchedule() {
        return this.http.get(this.route)
        .map(res => {
            return res.json()
        }).toPromise();
    }
}
