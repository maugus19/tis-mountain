import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Session } from '../models/session';
import { Group } from '../models/group';

@Injectable()
export class SessionService {
  route = 'http://localhost:8080/sessions';

  constructor(private http: Http) { }

  delteSession(sessionId) {
    return this.http.delete(this.route + '/' + sessionId)
    .map(res => {
      return res.json()
    }).toPromise();
  }

  registerSession(session) {
    return this.http.post(this.route, session)
    .map(res => {
      return res.json()
    }).toPromise();
  }

  getSessionsByGroup(groupId) {
    return this.http.get(this.route + '/group/' + groupId)
    .map(res => {
      return res.json()
    }).toPromise();
  }
}
