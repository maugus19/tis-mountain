import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: 'laboratory', loadChildren: './modules/laboratory/laboratory.module#LaboratoryModule' },
    { path: 'session', loadChildren: './modules/session/session.module#SessionModule' },
    { path: 'schedule', loadChildren: './modules/schedule/schedule.module#ScheduleModule' },
    { path: 'group', loadChildren: './modules/group/group.module#GroupModule' },
    { path: 'subject', loadChildren: './modules/subject/subject.module#SubjectModule' },
    { path: 'user', loadChildren: './modules/user/user.module#UserModule'}
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ],
})
export class AppRoutingModule { }
